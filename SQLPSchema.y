%{
	#include "util.h"
%}

%define api.value.type {cons_cell *}
%token IMPLIES OR AND NOT LE GE LT GT NE HAS MAX MIN AS ASC DESC MOD ASSIGN EQ STAR COMMA DOT

%token SIZE SELECTIVITY OVERLAP
%token FREQUENCY UNIT TIME SPACE

%token IDENTIFIER CONSTANT STRING_LITERAL SIZEOF

%token STORE STORING DYNAMIC STATIC OF TYPE ORDERED BY
%token INDEX LIST ARRAY BINARY TREE DISTRIBUTED POINTER

%token SCHEMA  CLASS ISA PROPERTIES CONSTRAINTS PROPERTY DISJOINT
%token FOREIGN KEY REFERENCES WITH
%token ON DETERMINED COVER QUERY GIVEN FROM SELECT WHERE ORDER
%token PRECOMPUTED ONE EXIST FOR ALL TRANSACTION INTCLASS STRCLASS OID
%token INTEGER REAL DOUBLEREAL STRING MAXLEN RANGE TO SELF
%token INSERT END CHANGE DELETE DECLARE RETURN UNION UNIONALL
%token LIMIT DISTINCT REF

%start SQLPSchema
%%
SQLPSchema
    : TableList ';'
	{
		printf("Input Schema\n");
		cons_cell *n = $1;
		printf("Printing Schema Tree\n");
        print_cons_tree(n);
        printf("\nPrinting Translated Schema Tree\n");
        translate_parse_tree(n);
	}
    ;

TableList
    : TableList ';' TableDefn  
    {
        printf("TableList\n");
        $$ = create_schema_operator($1, $3);
    }
    |  TableDefn
    {
        printf("TableList\n");
        $$ = create_schema_operator($1, NULL);
    }
    ;

TableDefn
    : CLASS ClassIdentifier '(' ClassSpecs ')'
    {
        printf("TableDefn\n");
        $$ = create_tabledefn_operator($2, $4);
    }
    ;

ClassSpecs
    : Self
    {
        printf("ClassSpecs\n");
        $$ = create_classpec_operator($1, NULL);
    }
    | ClassSpecs ',' ClassAttributes
    {
        printf("ClassSpecs\n");
        $$ = create_classpec_operator($1, $3);
    }
    | ClassSpecs ',' ForeignKeys
    {
        printf("ClassSpecs\n");
        $$ = create_classpec_operator($1, $3);
    }
    | ClassSpecs ',' Specialization
    {
        printf("ClassSpecs\n");
        $$ = create_classpec_operator($1, $3);
    }
    | ClassSpecs ',' DisjointConstraints
    {
        printf("ClassSpecs\n");
        $$ = create_classpec_operator($1, $3);
    }
    | ClassSpecs ',' OidExp
    {
        printf("ClassSpecs\n");
        $$ = create_classpec_operator($1, $3);
    }
    ;

    Self
    : SELF OID
    {
        printf("Create Self\n");
        $$ = create_self();
    }
    ;

ClassAttributes
    : AttributeIdentifier Type
    {
        printf("ClassAttributes\n");
        $$ = create_classattr_operator($1, $2);
    }
    | AttributeIdentifier ReferringType
    {
        printf("ClassAttributes\n");
        $$ = create_classattr_operator($1, $2);
    }
    ;

ForeignKeys
    : FOREIGN KEY AttributeIdentifier REFERENCES ClassIdentifier
    {
        printf("ForeignKeys\n");
        $$ = create_forkey_operator($3, $5);
    }
    ;

Specialization
    : ISA ClassIdentifier
    {
        printf("Specialization\n");
        $$ = create_specialization_operator($2);
    }
    ;

DisjointConstraints
    : DISJOINT WITH ClassIdentifier
    {
        printf("DisjointConstraints\n");
        $$ = create_disjoint_operator($3);
    }
    ;

OidExp
    : '('TableIdentifier')' REF '(' RefExp ')'
    {
        printf("OidExp\n");
        $$ = create_oidexp_operator($2, $6);
    }
    | '('SELF')' REF '(' RefExp ')'
    {
        printf("OidExp\n");
        $$ = create_oidexp_operator(create_self(), $6);
    }
    ;

RefExp
    : Col
    {
        printf("RefExp\n");
        $$ = create_refexp_operator($1, NULL);
    }
    | RefExp ',' Col
    {
        printf("RefExp\n");
        $$ = create_refexp_operator($1, $3);
    }
    | VarIdentifier
    {
        printf("RefExp\n");
        $$ = create_refexp_operator($1, NULL);
    }
    | RefExp ',' VarIdentifier
    {
        printf("RefExp\n");
        $$ = create_refexp_operator($1, $3);
    }
    ;

Col
    : VarIdentifier '.' AttrPath
    {
        printf("col\n");
        $$ = create_spcol($1, $3);
    }
    ;

AttrPath
	: AttributeIdentifier
	{ 
        printf("path id\n");
		$$ = create_pf($1, NULL);
	}
	| AttributeIdentifier '.' AttrPath
	{ 
        printf("Path Function\n");
		$$ = create_pf($1, $3);
	}
	;

Type
    : INTCLASS
    {
        printf("INTCLASS is |%s|", yytext);
        $$ = create_type(yytext);
    }
    | STRCLASS
    {
        printf("STRCLASS is |%s|", yytext);
        $$ = create_type(yytext);
    }
    | OID
    {
        printf("OID is |%s|", yytext);
        $$ = create_type(yytext);
    }
    ;

AttributeIdentifier
    : IDENTIFIER
    {
        printf("AttributeIdentifier is |%s| ", yytext);
		$$ = create_attr(yytext);
    }
    ;

TableIdentifier
	: IDENTIFIER
	{
		printf("|%s| ", yytext);
		$$ = create_table(yytext);
	}
    ;

VarIdentifier
	: IDENTIFIER
	{
		printf("|%s| ", yytext);
		$$ = create_var(yytext);
	}
    ;

ReferringType
    : IDENTIFIER
    {
        printf("ClassIdentifier is |%s| ", yytext);
		$$ = create_rt(yytext);
    }
    ;

ClassIdentifier
    : IDENTIFIER
    {
        printf("ClassIdentifier is |%s| ", yytext);
		$$ = create_class(yytext);
    }
    ;

