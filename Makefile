.PHONY: all clean
#CFLAGS = `pkg-config --cflags glib-2.0`
#LDLIBS = `pkg-config --libs glib-2.0`
all: SQLPSchema SQLPParser SQLPRefExp

SQLPParser: SQLPParser.c SQLPGrammar.y SQLPScanner.l util.o
	bison --verbose -d SQLPGrammar.y
	flex -D SQLPGrammar SQLPScanner.l
	gcc -w SQLPParser.c util.o -o SQLPParser
	rm -f lex.yy.c SQLPGrammar.tab.c SQLPGrammar.tab.h

SQLPSchema: SQLPSchema_main.c SQLPSchema.y SQLPScanner.l util.o
	bison -d SQLPSchema.y
	flex -D SQLPSchema SQLPScanner.l
	gcc -g -w SQLPSchema_main.c util.o -o SQLPSchema
	rm -f lex.yy.c SQLPSchema.tab.c SQLPSchema.tab.h

SQLPRefExp: SQLPRefExp_main.c SQLPRefExp.y SQLPScanner.l util.o
	bison -d SQLPRefExp.y
	flex -D SQLPRefExp SQLPScanner.l
	gcc -g -w SQLPRefExp_main.c util.o -o SQLPRefExp
	rm -f lex.yy.c SQLPRefExp.tab.c SQLPRefExp.tab.h

util.o: util.c util.h
	gcc -g -c util.c



Parser: parser.c util.o
	gcc -g -w parser.c util.o -o Parser

clean:
	rm -f lex.yy.c *.tab.c *.tab.h *.fasl Parser util.o

