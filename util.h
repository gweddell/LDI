#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

// Basic building blocks that contain char values
typedef struct atom {
	char* val;
} atom;

// A linked list like structure
// car points to element the node is holding
// cdr points to next cons cell in list or null
// is_atom determines if car is an atom
typedef struct cons_cell {
    void* car;
    struct cons_cell* cdr;
	bool is_atom;
} cons_cell;

// Creates a cons_cell
cons_cell* create_cons_cell(void* car, cons_cell* cdr);
// Creates an atom
atom* create_atom(char* val);
// Creates a cons_cell that has an atom as its car
cons_cell* create_cons_cell_w_atom(char* val, cons_cell* cdr);

// cons_list is a cons_cell wrapper that groups the same/similar "rules" in one list
// eg. a cons_list of Oids is an Oidlist (a list of pointers to cons_cells of rule type "Oid")
// Note that we can have a list of atoms. The only restriction on the type of the elements in
// a cons_list is that it has to conform to the same structure as the parser tree (ie. variable len tuple)

typedef struct cons_list {
    void *start;
    void *end;
    int len;
} cons_list;

cons_list* create_cons_list();
void append_item(cons_list* cons_list, char *item_name, void *item);
cons_cell* get_next_item(cons_cell* item);
cons_cell* get_item(cons_cell* first, int index);
void* get_item_val(cons_cell* item);

/*
Below comment block are functions that implement the following relational
algebra expressions for SQL queries:

<ra> := (comp <op> <term> <term> <ra>)
        | (atom <table> <var>)
        | (union-all <ra> <ra>)
        | (cross <ra> <ra>)
        | (proj ((rename <col> <col>)) <ra>)
        | (not <ra> <ra>)
        | (elim <ra>)
        | (limit n <ra> <ra>)
        | (eval <ra> <ra>)

<term> := <col> | <const>
<col> := <spcol> | <scol>
<spcol> := <var> "." <pf>   // for SQLP
<scol> := <var> "." <attr>  // for SQL
<pf> := <attr> <attr>
<var> := identifier
<attr> := identifier
<table> := identifier
<term> := identifier
<const> := identifier
<op> := [GT, GE, EQ, LE, LT]

*/

// The following are all helper functions to create the
// various relational algebra expressions
cons_cell* create_spcol(cons_cell* var, cons_cell* pf);
cons_cell* create_pf(cons_cell* attr, cons_cell* next_attr);
cons_cell* create_table(char *table);
cons_cell* create_term(cons_cell *term);
cons_cell* create_constant(char* constant);
cons_cell* create_col(char *col);
cons_cell* create_attr(char *attr);
cons_cell* create_var(char *var);
cons_cell* create_op(char *op);
cons_cell* create_comp_operator(cons_cell* op, cons_cell* term1, cons_cell* term2);
cons_cell* create_atom_operator(cons_cell* table, cons_cell* var);
cons_cell* create_union_all_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_union_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_cross_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_rename_operator(cons_cell* table, cons_cell* var);
cons_cell* create_proj_operator(cons_cell* assign, cons_cell* ra);
cons_cell* create_not_operator(cons_cell* ra);
cons_cell* create_exist_operator(cons_cell* ra1);
cons_cell* create_limit_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_elim_operator(cons_cell* ra1);
cons_cell* create_eval_operator(cons_cell* logic, cons_cell* cross);
cons_cell* create_and_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_or_operator(cons_cell* ra1, cons_cell* ra2);
cons_cell* create_as_operator(cons_cell* ra1, cons_cell* ra2);

// The following are all helper functions to create the 
// parse tree of the abstract relational schema
cons_cell* create_type(char *type);
cons_cell* create_class(char *class);
cons_cell* create_rt(char* class);
cons_cell* create_self();
cons_cell* create_disjoint_operator(cons_cell* classexp);
cons_cell* create_tabledefn_operator(cons_cell* name, cons_cell* specs);
cons_cell* create_schema_operator(cons_cell* table1, cons_cell* table2);
cons_cell* create_classpec_operator(cons_cell* exp1, cons_cell* );
cons_cell* create_classattr_operator(cons_cell* attr, cons_cell* type);
cons_cell* create_forkey_operator(cons_cell* attr, cons_cell* class);
cons_cell* create_specialization_operator(cons_cell* isa); 

// The following are all helper functions to create the 
// parse tree of the referring expression file

cons_cell* create_oidexplst_operator(cons_cell* explst, cons_cell* oidexp);
cons_cell* create_oidexp_operator(cons_cell* name, cons_cell* refexp);
cons_cell* create_refexp_operator(cons_cell* refexp, cons_cell* col);

// Prints an in order traversal of the tree
void print_cons_tree(cons_cell *root);
void print_cons_tree_helper(cons_cell *root, int indent);

// Helper functions to extract and rearrange information from schema 
// and referring expression parse trees
char *get_classid(cons_cell *classid);
char *get_classid_from_tb(cons_cell *tb_defn);
cons_list *list_tables(cons_cell *root);
cons_list *list_tables_helper(cons_cell *root, cons_list *tablelst);
void print_tablelist(cons_cell *root);
char *get_oid_attr(cons_cell *attr, cons_cell *type);
cons_list *list_oidexps(cons_cell *root);
cons_list *list_oidexps_helper(cons_cell *root, cons_list *oidexplst);
void print_oidexps(cons_cell *root);
bool validate_OidExp(cons_cell *root);

// Helpers for extracting information from cons_lists
bool is_self_refexp(cons_cell *first);
bool is_rt(cons_cell *first);
void prepend(char* s, const char* t);
char *get_reftb_name_rt(cons_cell *first);
char *get_refattr_name_rt(cons_cell *first);
char * get_reftb_name_refexp(cons_cell *first);
char * get_tb_name(cons_cell *first);
void modify_tb_name(cons_cell *tb_defn);
void modify_attr_name(cons_cell *class_attr, char* refattr_name);
cons_cell *deep_copy(cons_cell *first);
void add_attr(cons_cell *table, cons_cell *attr, char* refattr_name);


// Helper functions for translating abstract relational to relational
void translate_preprocess_refexp_helper(cons_cell *root, cons_list *refexps);
void translate_preprocess_refexp(cons_cell *table, cons_list *tb_refexp_lst);
void translate_preprocess_attr_helper(cons_cell *root, cons_list *attrs);
void translate_preprocess_attr(cons_cell *table, cons_list *tb_attr_lst);
void translate_preprocess_rt_helper(cons_cell *root, cons_list *rts);
void translate_preprocess_rt(cons_cell *table, cons_list *tb_rt_lst);
void translation_preprocess(cons_cell *root, cons_list *tb_refexp_lst, cons_list *tb_attr_lst, cons_list *tb_rt_lst);
void translate_table(cons_cell *table, cons_list *tb_attr_lst, cons_cell *refexp);
void translate_parse_tree(cons_cell *root);


#endif



