%{
	#include "util.h"
%}

%define api.value.type {cons_cell *}
%token IMPLIES OR AND NOT LE GE LT GT NE HAS MAX MIN AS ASC DESC MOD ASSIGN EQ STAR COMMA DOT

%token SIZE SELECTIVITY OVERLAP
%token FREQUENCY UNIT TIME SPACE

%token IDENTIFIER CONSTANT STRING_LITERAL SIZEOF

%token STORE STORING DYNAMIC STATIC OF TYPE ORDERED BY
%token INDEX LIST ARRAY BINARY TREE DISTRIBUTED POINTER

%token SCHEMA  CLASS ISA PROPERTIES CONSTRAINTS PROPERTY DISJOINT
%token FOREIGN KEY REFERENCES WITH
%token ON DETERMINED COVER QUERY GIVEN FROM SELECT WHERE ORDER
%token PRECOMPUTED ONE EXIST FOR ALL TRANSACTION INTCLASS STRCLASS OID
%token INTEGER REAL DOUBLEREAL STRING MAXLEN RANGE TO SELF
%token INSERT END CHANGE DELETE DECLARE RETURN UNION UNIONALL
%token LIMIT DISTINCT  REF

%start SQLPRefExp
%%
SQLPRefExp
    : OidExpLst
	{
		printf("Input RefExp\n");
		cons_cell *n = $1;
		printf("Printing RefExp Tree\n");
		print_cons_tree(n);
	}
    ;

OidExpLst
    : OidExpLst ';' OidExp  
    {
        printf("OidExpLst\n");
        $$ = create_oidexplst_operator($1, $3);
    }
    |  OidExp
    {
        printf("OidExp\n");
        $$ = create_oidexplst_operator($1, NULL);
    }
    ;

OidExp
    : TableIdentifier REF '(' RefExp ')'
    {
        printf("OidExp\n");
        $$ = create_oidexp_operator($1, $4);
    }
    ;

RefExp
    : Col
    {
        printf("RefExp\n");
        $$ = create_refexp_operator($1, NULL);
    }
    | RefExp ',' Col
    {
        printf("RefExp\n");
        $$ = create_refexp_operator($1, $3);
    }
    | VarIdentifier
    {
        printf("RefExp\n");
        $$ = create_refexp_operator($1, NULL);
    }
    | RefExp ',' VarIdentifier
    {
        printf("RefExp\n");
        $$ = create_refexp_operator($1, $3);
    }
    ;

Col
    : VarIdentifier '.' AttrPath
    {
        printf("col\n");
        $$ = create_spcol($1, $3);
    }

AttrPath
	: AttrIdentifier
	{ 
        printf("path id\n");
		$$ = create_pf($1, NULL);
	}
	| AttrIdentifier '.' AttrPath
	{ 
        printf("Path Function\n");
		$$ = create_pf($1, $3);
	}
	;

VarIdentifier
	: IDENTIFIER
    {
		printf("|%s| ", yytext);
		$$ = create_var(yytext);
	}

TableIdentifier
	: IDENTIFIER
	{
		printf("|%s| ", yytext);
		$$ = create_table(yytext);
	}

AttrIdentifier
	: IDENTIFIER
	{
		printf("|%s| ", yytext);
		$$ = create_attr(yytext);
	}

