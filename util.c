#include "util.h"

// Create and Manipulate cons_cells
cons_cell* create_cons_cell(void* car, cons_cell* cdr) {
	cons_cell* new_cons = malloc(sizeof(cons_cell));
	new_cons->car = car;
	new_cons->cdr = cdr;
	new_cons->is_atom = false;
	return new_cons;
}


atom* create_atom(char* val) {
	printf("create_atom: %s\n", val);
	atom* new_atom = malloc(sizeof(atom));
	new_atom->val = malloc(sizeof(char) * (strlen(val) + 1));
	strcpy(new_atom->val, val);
	return new_atom;
}


cons_cell* create_cons_cell_w_atom(char* val, cons_cell* cdr) {
	atom* new_atom = create_atom(val);
	cons_cell* new_cons = malloc(sizeof(cons_cell));
	new_cons->car = new_atom;
	new_cons->cdr = cdr;
	new_cons->is_atom = true;
	return new_cons;
}

//*************************************************************************************************
// Create and Manipulate cons_lists (basically cons_cells)

cons_list* create_cons_list() {
	cons_list* conslst = malloc(sizeof(cons_list));
	conslst->len = 0;
	conslst->start = NULL;
	conslst->end = NULL;
	return conslst;
}

void append_item(cons_list* cons_list, char *item_name, void *item) {
	cons_cell *new_conslst;
	if (item_name != NULL) {   //when we need additional information, we add an extra atom to each item
	    cons_cell *new_item = create_cons_cell_w_atom(item_name, create_cons_cell(item, NULL));
        new_conslst = create_cons_cell(new_item, NULL);
	} else {  //if we don't need additional info, set item_name to null
        new_conslst = create_cons_cell(item, NULL);
	}

	if (cons_list->len == 0) {
		char operator[8] = "CONSLST\0";
	    cons_cell* operator_cons = create_cons_cell_w_atom(operator, new_conslst);
		cons_list->start = operator_cons;
		cons_list->end = new_conslst;
	} else {
        ((cons_cell*)(cons_list->end))->cdr = new_conslst;
	    cons_list->end = new_conslst;
	}

	++cons_list->len;
}

void* get_item_val(cons_cell* item) { //list with additional info
	return (void*)(item->car);
}

cons_cell* get_next_item(cons_cell* item) {
	return item->cdr;
}

cons_cell* get_item(cons_cell* first, int index) { //first must be cons_cell containing rule
	cons_cell* curr = first;
	while(index>0) {
        curr = get_next_item(curr);
		--index;
	} 
	return curr;
}

//*************************************************************************************************
// SQLP Query helpers
cons_cell* create_spcol(cons_cell* var, cons_cell* pf) {
	cons_cell* var_cons = create_cons_cell(var, pf);
	char operator[6] = "SPCOL\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, var_cons);
	return operator_cons;
}

cons_cell* create_pf(cons_cell* attr, cons_cell* next_attr) {
	cons_cell* attr_cons = create_cons_cell(attr, next_attr);
	char operator[3] = "PF\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, attr_cons);
	return operator_cons;
}

cons_cell* create_table(char *table) {
	cons_cell* table_cons = create_cons_cell_w_atom(table, NULL);
	char operator[6] = "TABLE\0";
	return create_cons_cell_w_atom(operator, table_cons);
}

cons_cell* create_term(cons_cell* term) {
	cons_cell* term_cons = create_cons_cell(term, NULL);
	char operator[5] = "TERM\0";
	return create_cons_cell_w_atom(operator, term_cons);
}

cons_cell* create_constant(char* constant) {
	cons_cell* constant_cons = create_cons_cell_w_atom(constant, NULL);
	char operator[6] = "CONST\0";
	return create_cons_cell_w_atom(operator, constant_cons);
}

cons_cell* create_col(char *col) {
	cons_cell* col_cons = create_cons_cell_w_atom(col, NULL);
	char operator[4] = "COL\0";
	return create_cons_cell_w_atom(operator, col_cons);
}

cons_cell* create_attr(char *attr) {
	cons_cell* attr_cons = create_cons_cell_w_atom(attr, NULL);
	char operator[5] = "ATTR\0";
	return create_cons_cell_w_atom(operator, attr_cons);
}

cons_cell* create_var(char *var) {
	cons_cell* var_cons = create_cons_cell_w_atom(var, NULL);
	char operator[4] = "VAR\0";
	return create_cons_cell_w_atom(operator, var_cons);
}

cons_cell* create_op(char *op) {
	cons_cell* op_cons = create_cons_cell_w_atom(op, NULL);
	char operator[3] = "OP\0";
	return create_cons_cell_w_atom(operator, op_cons);
}

cons_cell* create_comp_operator(cons_cell* op, cons_cell* term1, cons_cell* term2) {
	cons_cell* term2_cons = create_cons_cell(term2, NULL);
	cons_cell* term1_cons = create_cons_cell(term1, term2_cons);
	cons_cell* op_cons = create_cons_cell(op, term1_cons);
	char operator[5] = "COMP\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, op_cons);
	return operator_cons;
}


cons_cell* create_atom_operator(cons_cell* table, cons_cell* var) {
	cons_cell* var_cons = create_cons_cell(var, NULL);
	cons_cell* tab_name_cons = create_cons_cell(table, var_cons);
	char operator[5] = "ATOM\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, tab_name_cons);
	return operator_cons;
}

cons_cell* create_union_all_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[10] = "UNION_ALL\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_union_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[6] = "UNION\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_and_operator(cons_cell* ra1, cons_cell* ra2) {
	printf("create_and_operator called \n");
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[4] = "AND\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_or_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[3] = "OR\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_njoin_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[6] = "NJOIN\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_cross_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[6] = "CROSS\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_rename_operator(cons_cell* table, cons_cell* var) {
	cons_cell* original_cons = create_cons_cell(table, var);
	char operator[7] = "RENAME\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, original_cons);
	return operator_cons;
}

cons_cell* create_proj_operator(cons_cell* assign, cons_cell* ra) {
	cons_cell* ra_cons = create_cons_cell(ra, NULL);
	cons_cell* assign_cons = create_cons_cell(assign, ra_cons);
	char operator[5] = "PROJ\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, assign_cons);
	return operator_cons;
}

cons_cell* create_not_operator(cons_cell* ra) {
	cons_cell* ra_cons = create_cons_cell(ra, NULL);
	char operator[4] = "NOT\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra_cons);
	return operator_cons;
}

cons_cell* create_exist_operator(cons_cell* ra1) {
	cons_cell* ra1_cons = create_cons_cell(ra1, NULL);
	char operator[6] = "EXIST\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return create_cons_cell_w_atom(operator, ra1_cons);
}


cons_cell* create_limit_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[6] = "LIMIT\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_elim_operator(cons_cell* ra1) {
	cons_cell* ra1_cons = create_cons_cell(ra1, NULL);
	char operator[5] = "ELIM\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_eval_operator(cons_cell* logic, cons_cell* cross) {
	cons_cell* cross_cons = create_cons_cell(cross, NULL);
	cons_cell* var_cons = create_cons_cell(logic, cross_cons);
	char operator[5] = "EVAL\0";
	return create_cons_cell_w_atom(operator, var_cons);
}

cons_cell* create_as_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[3] = "AS\0";
	return create_cons_cell_w_atom(operator, ra1_cons);
}

//*************************************************************************************************
// SQLP Schema helpers

cons_cell* create_type(char* type) {
	cons_cell* type_cons = create_cons_cell_w_atom(type, NULL);
	char operator[5] = "TYPE\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, type_cons);
	return operator_cons;
}

cons_cell* create_class(char* class) {
	cons_cell* class_cons = create_cons_cell_w_atom(class, NULL);
	char operator[6] = "CLASS\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, class_cons);
    return operator_cons;
}

cons_cell* create_rt(char* class) {
	cons_cell* class_cons = create_cons_cell_w_atom(class, NULL);
	char operator[3] = "RT\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, class_cons);
    return operator_cons;
}

cons_cell* create_self() {
	char self[5] = "Self\0";
	cons_cell* self_cons = create_cons_cell_w_atom(self, NULL);
	char operator[5] = "SELF\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, self_cons);
    return operator_cons;
}

cons_cell* create_disjoint_operator(cons_cell* classexp) {
	cons_cell* classexp_cons = create_cons_cell(classexp, NULL);
	char operator[9] = "DISJOINT\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, classexp_cons);
	return operator_cons;
}

cons_cell* create_tabledefn_operator(cons_cell* name, cons_cell* specs) {
	cons_cell* specs_cons = create_cons_cell(specs, NULL);
	cons_cell* name_specs_cons = create_cons_cell(name, specs_cons);
	char operator[11] = "TABLEDEFN\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, name_specs_cons);
	return operator_cons;
}

cons_cell* create_schema_operator(cons_cell* table1, cons_cell* table2) {
	cons_cell* exp1_cons = NULL;
	cons_cell* exp2_cons = NULL;
	if (table2 == NULL) {
	  exp1_cons = create_cons_cell(table1, NULL);
	} else {
      exp2_cons = create_cons_cell(table2, NULL);
	  exp1_cons = create_cons_cell(table1, exp2_cons);
	}
	char operator[7] = "SCHEMA\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, exp1_cons);
	return operator_cons;
}

cons_cell* create_classpec_operator(cons_cell* exp1, cons_cell* exp2) {
	cons_cell* exp1_cons = NULL;
	cons_cell* exp2_cons = NULL;
	if (exp2 == NULL) {
	  exp1_cons = create_cons_cell(exp1, NULL);
	} else {
      exp2_cons = create_cons_cell(exp2, NULL);
	  exp1_cons = create_cons_cell(exp1, exp2_cons);
	}
	char operator[9] = "CLASSPEC\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, exp1_cons);
	return operator_cons;
}

cons_cell* create_classattr_operator(cons_cell* attr, cons_cell* type) {
	cons_cell* type_cons = create_cons_cell(type, NULL);
	cons_cell* attr_type_cons = create_cons_cell(attr, type_cons);
	char operator[10] = "CLASSATTR\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, attr_type_cons);
	return operator_cons;
}

cons_cell* create_forkey_operator(cons_cell* attr, cons_cell* class) {
	cons_cell* class_cons = create_cons_cell(class, NULL);
	cons_cell* attr_class_cons = create_cons_cell(attr, class_cons);
	char operator[7] = "FORKEY\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, attr_class_cons);
	return operator_cons;
}

cons_cell* create_specialization_operator(cons_cell* isa) {
	cons_cell* isa_cons = create_cons_cell(isa, NULL);
	char operator[4] = "ISA\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, isa_cons);
	return operator_cons;
}

//*************************************************************************************************
// SQLP RefExp helpers

cons_cell* create_oidexplst_operator(cons_cell* explst, cons_cell* oidexp) {
	cons_cell* exp1_cons = NULL;
	cons_cell* exp2_cons = NULL;
	if (oidexp == NULL) {
	  exp1_cons = create_cons_cell(explst, NULL);
	} else {
      exp2_cons = create_cons_cell(oidexp, NULL);
	  exp1_cons = create_cons_cell(explst, exp2_cons);
	}
	char operator[10] = "OIDEXPLST\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, exp1_cons);
	return operator_cons;
}

cons_cell* create_oidexp_operator(cons_cell* name, cons_cell* refexp) {
	cons_cell* refexp_cons = create_cons_cell(refexp, NULL);
	cons_cell* name_refexp_cons = create_cons_cell(name, refexp_cons);
	char operator[7] = "OIDEXP\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, name_refexp_cons);
	return operator_cons;
}

cons_cell* create_refexp_operator(cons_cell* refexp, cons_cell* col) {
	cons_cell* exp1_cons = NULL;
	cons_cell* exp2_cons = NULL;
	if (col == NULL) {
	  exp1_cons = create_cons_cell(refexp, NULL);
	} else {
      exp2_cons = create_cons_cell(col, NULL);
	  exp1_cons = create_cons_cell(refexp, exp2_cons);
	}
	char operator[7] = "REFEXP\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, exp1_cons);
	return operator_cons;
}

//*************************************************************************************************
// Print Parser Tree helpers

void print_cons_tree(cons_cell *root) {
	print_cons_tree_helper(root, 0);
}

void print_cons_tree_helper(cons_cell *root, int indent) {
	if (root == NULL) {
		return;
	}

	if (root->is_atom) {
		for (int i = 0; i < indent; i++) {
			printf("  ");
		}
		printf("%s", ((atom*)root->car)->val);
		printf("\n");
		print_cons_tree_helper(root->cdr, indent+1);
	} else {
		cons_cell* tmp = (cons_cell*)root->car;
		print_cons_tree_helper((cons_cell*)root->car, indent);
		print_cons_tree_helper(root->cdr, indent);
	}
	return;
} 

//*************************************************************************************************
// Helpers for extracting information from cons_cells

char *get_classid_from_tb(cons_cell *tb_defn) {
    return get_classid(((cons_cell*)tb_defn->car)->cdr);
}

char *get_classid(cons_cell *classid) {
    if (classid->is_atom) {
		return ((atom*)classid->car)->val;
	} else {
		return NULL;
	}
}

char *get_oid_attr(cons_cell *attr, cons_cell *type) {
    if (attr->is_atom && type->is_atom) {
		cons_cell* attr_atom = attr->cdr;
		cons_cell* type_atom = type->cdr;
		bool is_oid = strcmp(((atom*)type_atom->car)->val, "Oid");
		if (is_oid == 0) {
			return ((atom*)attr_atom->car)->val;
		} else {
			return NULL;
		}
	} else {
		printf("cannot find attr and type\n");
		return NULL;
	}
}

bool is_self_refexp(cons_cell *first) {
    cons_cell *self_atom = (cons_cell*)((cons_cell*)(first->cdr)->car)->car;
	bool result = strcmp(((atom *)self_atom)->val, "SELF");
	if (result == 0) {
		return true;
	} else {
		return false;
	}
}

bool is_rt(cons_cell *first) {
	bool is_attr = strcmp(((atom*)first->car)->val, "FORKEY");
	if (is_attr == 0) {
		return true;
	} 
	return false;
}

void prepend(char* s, const char* t){
    size_t len = strlen(t);
    memmove(s + len, s, strlen(s) + 1);
    memcpy(s, t, len);
}

char *get_reftb_name_rt(cons_cell *first) {
    cons_cell *rt_atom = (cons_cell*)((cons_cell*)((cons_cell*)((cons_cell*)(first->cdr)->cdr)->car)->cdr)->car;
	return ((atom *)rt_atom)->val;
}

char *get_refattr_name_rt(cons_cell *first) {
	cons_cell *attr_atom = (cons_cell*)((cons_cell*)((cons_cell*)(first->cdr)->car)->cdr)->car;
	return ((atom *)attr_atom)->val;
}

char * get_reftb_name_refexp(cons_cell *first) {
    cons_cell *table_atom = (cons_cell*)((cons_cell*)((cons_cell*)(first->cdr)->car)->cdr)->car;
	return ((atom *)table_atom)->val;
}

char * get_tb_name(cons_cell *first) {
	cons_cell *tb_name_atom = (cons_cell*)((cons_cell*)((cons_cell*)(first->cdr)->car)->cdr)->car;
	return ((atom *)tb_name_atom)->val;
}

void modify_tb_name(cons_cell *tb_defn) {
	cons_cell *tb_name_atom = (cons_cell*)((cons_cell*)((cons_cell*)(tb_defn->cdr)->car)->cdr)->car;
	char *tb_name = ((atom *)tb_name_atom)->val;
	strcat(tb_name, "_0");
}

void modify_attr_name(cons_cell *class_attr, char* refattr_name) {
	cons_cell *tb_name_atom = (cons_cell*)((cons_cell*)((cons_cell*)(class_attr->cdr)->car)->cdr)->car;
	char *attr_name = ((atom *)tb_name_atom)->val;
	prepend(attr_name, "_");
	prepend(attr_name, refattr_name);
}

cons_cell *deep_copy(cons_cell *first) {
  if (first == NULL) {
	  return NULL;
  } else if (first->is_atom) {
    char *atom_str = ((atom *)first->car)->val;
	cons_cell *exp = deep_copy(first->cdr);
	return create_cons_cell_w_atom(atom_str, exp);
  } else {
    cons_cell *exp1 = deep_copy(first->car);
	cons_cell *exp2 = deep_copy(first->cdr);
	return create_cons_cell(exp1, exp2);
  }
}

void add_attr(cons_cell *table, cons_cell *attr, char* refattr_name) {
    cons_cell *new_attr = deep_copy(attr);
	modify_attr_name(new_attr, refattr_name);
	cons_cell *root_classpec_ptr = get_item(table->cdr, 1);
	cons_cell *old_classpec = get_item_val(root_classpec_ptr);
    cons_cell *new_classpec = create_classpec_operator(old_classpec, new_attr);
    
	table->cdr->cdr->car = new_classpec;
}

//*************************************************************************************************
// Schema + RefExp Validation (basic preliminary checks)

cons_list *list_tables(cons_cell *root) {
  cons_list *tablelst = create_cons_list();
  return list_tables_helper(root, tablelst);
}

cons_list *list_tables_helper(cons_cell *root, cons_list *tablelst) {
    if (root == NULL) {
		return NULL;
	}

	if (root->is_atom) {
		if (strcmp(((atom*)root->car)->val, "TABLEDEFN") == 0) {
          char *classid = get_classid_from_tb(root->cdr);  
		  append_item(tablelst, classid, root); //change to store pointer to table def'n
		}
		list_tables_helper(root->cdr, tablelst);
	} else {
		cons_cell* tmp = (cons_cell*)root->car;
		list_tables_helper((cons_cell*)root->car, tablelst);
		list_tables_helper(root->cdr, tablelst);
	}
	return tablelst;
}

cons_list *list_oidexps(cons_cell *root) {
  cons_list *oidexplst = create_cons_list();
  return list_oidexps_helper(root, oidexplst);
}

cons_list *list_oidexps_helper(cons_cell *root, cons_list *oidexplst) {
    if (root == NULL) {
		return NULL;
	}

	if (root->is_atom) {
		if ( strcmp(((atom*)root->car)->val, "OIDEXP") == 0) {
          char *attr = get_reftb_name_refexp(root);
		  if (attr != NULL) {
            append_item(oidexplst, attr, root);
		  }
		}
		list_oidexps_helper(root->cdr, oidexplst);
	} else {
		list_oidexps_helper((cons_cell*)root->car, oidexplst);
		list_oidexps_helper(root->cdr, oidexplst);
	}
	return oidexplst;
}

void print_tablelist(cons_cell *root) {
	cons_list *tablelst = list_tables(root);
	int len = tablelst->len;
	printf("tables:\n");
	cons_cell *curr = ((cons_cell*)(tablelst->start))->cdr;
	for (int i=0; i<len; ++i) {
		printf("%s, ", ((atom*)(((cons_cell*)(curr->car))->car))->val);
		curr = get_next_item(curr);
	}
	printf("\n");
}

void print_oidexps(cons_cell *root) {
	cons_list * oidexplst = list_oidexps(root);
	int len = oidexplst->len;
	printf("oidexps:\n");

	cons_cell *curr = NULL;
	if (len > 0) {
		curr = ((cons_cell*)(oidexplst->start))->cdr;
	} 

	for (int i=0; i<len; ++i) {
		printf("%s, ", ((atom*)(((cons_cell*)(curr->car))->car))->val);
		curr = get_next_item(curr);
	}
	printf("\n");
}

bool validate_OidExp(cons_cell *root) {
   cons_list *oidexplst = list_oidexps(root);
   int olen = oidexplst->len;
   cons_list *tablelst = list_tables(root);
   int tlen = tablelst->len;

   cons_cell *curr_oid = NULL;
   if (olen > 0) {
		curr_oid = ((cons_cell*)(oidexplst->start))->cdr;
	} 

   for(int i=0; i<olen; ++i) {
	   bool found = false;
	   char *oiditem = ((atom*)(((cons_cell*)(curr_oid->car))->car))->val;
	   cons_cell *curr_tb = (cons_cell*)((cons_cell*)(tablelst->start))->cdr;
	   for (int j=0; j<tlen; ++j) {
		   char *tbitem = ((atom*)(((cons_cell*)(curr_tb->car))->car))->val;
		   if (strcmp(oiditem, tbitem)==0 || strcmp(oiditem, "Self")==0) {
			   found = true;
		   }
		   curr_tb = get_next_item(curr_tb);
	   }
	   if (!found) {
		   printf("Oidexp does not match any table\n");
		   return false;
	   }
	   curr_oid = get_next_item(curr_oid);
   }
   printf("All Oidexps resolved\n");
   return true;
}

//*************************************************************************************************
// Schema + RefExp translation from abstract relational to relational 

//general workflow
//

void translate_preprocess_refexp_helper(cons_cell *root, cons_list *refexps) {
    if (root == NULL) {
		return;
	}

	if (root->is_atom) {
		if (strcmp(((atom*)root->car)->val, "OIDEXP") == 0) {
		  append_item(refexps, NULL, root); 
		}
		translate_preprocess_refexp_helper(root->cdr, refexps);
	} else {
		translate_preprocess_refexp_helper((cons_cell*)root->car, refexps);
		translate_preprocess_refexp_helper(root->cdr, refexps);
	}
}

void translate_preprocess_refexp(cons_cell *table, cons_list *tb_refexp_lst) {
	printf("Preprocessing refexp for table: %s\n", get_tb_name(table));
    cons_list *new_refexp_lst = create_cons_list();
    append_item(new_refexp_lst, NULL, table);

    cons_cell *curr = table;
	translate_preprocess_refexp_helper(curr, new_refexp_lst);
	append_item(tb_refexp_lst, NULL, new_refexp_lst);
}

void translate_preprocess_attr_helper(cons_cell *root, cons_list *attrs) {
    if (root == NULL) {
		return;
	}

	if (root->is_atom) {
		if (strcmp(((atom*)root->car)->val, "CLASSATTR") == 0) {
		  append_item(attrs, NULL, root); //change to store pointer to table def'n
		}
		translate_preprocess_attr_helper(root->cdr, attrs);
	} else {
		translate_preprocess_attr_helper((cons_cell*)root->car, attrs);
		translate_preprocess_attr_helper(root->cdr, attrs);
	}
}

void translate_preprocess_attr(cons_cell *table, cons_list *tb_attr_lst) {
	printf("Preprocessing attr for table: %s\n", get_tb_name(table));
	cons_list *new_attr_lst = create_cons_list();
    append_item(new_attr_lst, NULL, table);

    cons_cell *curr = table;
	translate_preprocess_attr_helper(curr, new_attr_lst);
	append_item(tb_attr_lst, NULL, new_attr_lst);
}

void translate_preprocess_rt_helper(cons_cell *root, cons_list *rts) {
    if (root == NULL) {
		return;
	}

	if (root->is_atom) {
		if (is_rt(root)) {
		  append_item(rts, NULL, root); //change to store pointer to table def'n
		}
		translate_preprocess_rt_helper(root->cdr, rts);
	} else {
		translate_preprocess_rt_helper((cons_cell*)root->car, rts);
		translate_preprocess_rt_helper(root->cdr, rts);
	}
}

void translate_preprocess_rt(cons_cell *table, cons_list *tb_rt_lst) {
	printf("Preprocessing rt for table: %s\n", get_tb_name(table));
	cons_list *new_rt_lst = create_cons_list();
    append_item(new_rt_lst, NULL, table);

    cons_cell *curr = table;
	translate_preprocess_rt_helper(curr, new_rt_lst);
	append_item(tb_rt_lst, NULL, new_rt_lst);
}

void translation_preprocess(cons_cell *root, cons_list *tb_refexp_lst, cons_list *tb_attr_lst, 
                            cons_list *tb_rt_lst) {
	//go through all tables
	//at each table do:
	//  1. make new refexp list 
	//  2. make new attr list
	printf("Translation Preprocess Start\n");
    cons_cell *curr_item = root; // curr_item is always Schema node
	while (curr_item != NULL) {
		if (curr_item->cdr->cdr != NULL) { 
		    printf("Preprocessing Table: %s\n", get_tb_name(get_item_val(get_item(curr_item->cdr, 1))));
			translate_preprocess_refexp(get_item_val(get_item(curr_item->cdr, 1)), tb_refexp_lst); 
			translate_preprocess_attr(get_item_val(get_item(curr_item->cdr, 1)), tb_attr_lst);
			translate_preprocess_rt(get_item_val(get_item(curr_item->cdr, 1)), tb_rt_lst);
			curr_item = get_item_val(get_item(curr_item->cdr, 0));
		} else {
			printf("Preprocessing Table: %s\n", get_tb_name(get_item_val(get_item(curr_item->cdr, 0))));
			translate_preprocess_refexp(get_item_val(get_item(curr_item->cdr, 0)), tb_refexp_lst);
			translate_preprocess_attr(get_item_val(get_item(curr_item->cdr, 0)), tb_attr_lst);
			translate_preprocess_rt(get_item_val(get_item(curr_item->cdr, 0)), tb_rt_lst);
			break;
		}
	}
}

void translate_table(cons_cell *table, cons_list *tb_attr_lst, cons_cell *rt) {

	//find the corresponding attr_lst in tb_attr_lst
    char *reftb_name = get_reftb_name_rt(rt); 
	char *refattr_name = get_refattr_name_rt(rt); 
    int tb_attr_lst_len = tb_attr_lst->len;
	cons_list * attr_lst;


	for (int i=0; i<tb_attr_lst_len; ++i) {  
		cons_list *curr_attr_lst = get_item_val(get_item(((cons_cell*)(tb_attr_lst->start))->cdr, i));
		cons_cell *ref_tb = get_item_val(get_item(((cons_cell*)(curr_attr_lst->start))->cdr, 0));
		char *currtb_name = get_tb_name(ref_tb);

		if (strcmp(reftb_name, currtb_name) == 0) {
			attr_lst = curr_attr_lst;
			break;
		} 
	}
	//change table name
    modify_tb_name(table);

	//add attributes (attr name appended)
    int attr_lst_len = attr_lst->len;
	for (int i=1; i<attr_lst_len; ++i) { //starts at 1 since 0 is table def'n 
      cons_cell *curr_attr = get_item_val(get_item(((cons_cell*)(attr_lst->start))->cdr, i));
	  add_attr(table, curr_attr, refattr_name);
	}

	//remove old refexp(not neccessary for now)
}

void translate_parse_tree(cons_cell *root) {
    cons_list *tb_refexp_lst = create_cons_list(); //a conslst of conslst of (table refexp refexp....)
	cons_list *tb_attr_lst = create_cons_list(); //a conslst of conslst of (table attr attr....)
	cons_list *tb_rt_lst = create_cons_list();  //a conslst of conslst of (table rt rt....)

	translation_preprocess(root, tb_refexp_lst, tb_attr_lst, tb_rt_lst);

	int tb_refexp_lst_len = tb_refexp_lst->len;
	int tb_attr_lst_len = tb_attr_lst->len;
	int tb_rt_lst_len = tb_rt_lst->len;


	for (int i=0; i<tb_rt_lst_len;++i) {  //while + get_next is more efficient
      cons_list *rt_lst =  get_item_val(get_item(((cons_cell*)(tb_rt_lst->start))->cdr, i));
	  int rt_lst_len = rt_lst->len;  
	  cons_cell *reftable = get_item_val(get_item(((cons_cell*)(rt_lst->start))->cdr, 0));  

	  for (int j=1; j<rt_lst_len;++j) { //index starts at 1 since 0 contains table info
		  cons_cell *rt =  get_item_val(get_item(((cons_cell*)(rt_lst->start))->cdr, j));
		  translate_table(reftable, tb_attr_lst, rt); 
	  }
	}

	print_cons_tree(root);
}

